# Blog Application
#### Users module:
-----------------
1. Users can register to Blog.
2. Registered user can edit their personal details if required.
3. Registered user can de-register their user info.

#### Blog Posts module:
--------------------
1. Registered users can write some blog posts and save them on to the server.
2. Anyone can get all the list of blog posts.
3. Registered users can edit their own blog posts. (in order to edit blog post, posted user should be same as editing user.)
4. One user could not edit Blog posts of another user.
5. **postOwner** or a username with **Admin** privileges only can delete the posts.


Sample URL for demo:

https://blogs.perficient.com/2020/06/17/strengthening-our-global-delivery-capabilities-with-psl/

sample User details structure will be as follows:
```json
{
	userName: "",
	fullName: "",	
	email: "",
	password: "",
	isAdmin: true  /  false.
}
```

a sample Blog Post structure is defined here:
```json
{
	postTitle: "", // Blog Post title
	postContent: "", // Blog post content
	postedDate: "", // date and time when blog post is saved
	postedOwner: ""  // Username who posted this Blog post information.
	tags: ["Global delivery", "near shore delivery"],
	category: ["News"],
	viewsCount: 0  //We will be incrementing this count when the post is loaded.
}
```
  

